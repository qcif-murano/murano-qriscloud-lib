Murano QRIScloudLib
===================

This is the source for the QRIScloud libraries Murano package running on the NeCTAR cloud.

A Makefile is included to help the build process.

You will need your NeCTAR cloud credentials loaded and the Murano CLI tools
available in your path.
